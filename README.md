# dfhash_driver

Jupyter notebook with a prototype to hash obfuscate columns in a dataframe

Test of code to obfuscate columns in a dataframe.     Goal is allow a clear data to be rehash over time
and allow the hash data to be sabel and joinable. 

see hashdf 
   - looc at clearcols to specify which columns remain in clear

Algorithm defaults to sha3_224 - nice mix of speed and security.  
Why?  When used with a column used as a salt: time to create full space
    
   40 bits  - 0.5 days
   64 .     - 27k years.     Plenty good enough a obfuscation / privacy algorithm. 
   
   Note:  the values be hashed are to obfusacate privacy data - so we assaume there 
   will be many duplicate values .  (first names, last names, city, state, etc.)
   
   Data from:  eBACS: ECRYPT Benchmarking of Cryptographic Systems |https://bench.cr.yp.to/results-hash.html
   Algorithm used from https://crypto.stackexchange.com/questions/59045/benchmark-hash-rate-for-sha-3-shake128
""" 